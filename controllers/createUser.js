import postUser from "./postUser.js";

/* get DOM elements */
const inputUserName = document.querySelector("#userName");
const inputName = document.querySelector("#name");
const inputCpf = document.querySelector("#cpf")
const inputEmail = document.querySelector("#email");
const inputDate = document.querySelector("#date");
const inputPassword = document.querySelector("#password");

const span = document.querySelector(".error");

const cadastro = document.querySelector("#cadastro");


console.log("Hello Word!");

cadastro.addEventListener("click", async (event) => {
  event.preventDefault();
  const userName = inputUserName.value;
  const name = inputName.value;
  const cpf = inputCpf.value;
  const email = inputEmail.value;
  const date = inputDate.value;
  const password = inputPassword.value;
  

  /* verificar se os campos estão preenchidos */
  if (userName === "" || name === "" || cpf === "" || email === "" || date === "" || password === "") {
    span.classList.remove("invisible");
    console.log("preencha os campos")
  } else {
    span.classList.add("invisible");
    await postUser(userName, name, cpf, email, date, password);
  }
});


