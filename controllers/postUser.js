/* requisição assíncrona para criar uma nova Chore */
const postUser = async (userName, name, cpf, email, date,  password) => {
  try {
    const response = await fetch("http://localhost:3000/users", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify({
        Userame: `${userName}`,
        name: `${name}`,
        cpf: `${cpf}`,
        email: `${email}`,
        date: `${date}`,
        password: `${password}`,
      }),
    });
    const content = await response.json();
    console.log("Os arquivos foram pro json!");
    return content;
  } catch (error) {
    console.log(error);
  }
};

export default postUser;
