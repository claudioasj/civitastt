# Civitasa
claudio

Este projeto foi desenvolvido durante o TT da EJCM e foi proposto a criação do **CIVITAS**.
<br><br><br><br>
<p align="center" >
<img src="./assets/civitaslogo.png">
<img src="./assets/letralogo.png">
</p>
<br><br>
> obs: Civitas é um marketplace ficticio desenvolvido para ofertar produtos e serviços a uma comunidade específica.

<br><br><br><br><br>
## Tecnologias utilizadas

<br><br>
<p align="center">
  <img src="https://cdn-icons-png.flaticon.com/512/5968/5968267.png" width="140">
  <img src="https://cdn-icons-png.flaticon.com/512/5968/5968242.png" width="140">
  <img src="https://cdn-icons-png.flaticon.com/512/5968/5968292.png" width="130">
</p>
<br><br>
Além disso, temos  uso de <a href="https://developer.mozilla.org/pt-BR/docs/Web/API/Fetch_API">Fetch API</a> e <a href="https://www.npmjs.com/package/json-server"> JSON server</a>. <br><br><br>

### Como utilizar o projeto
crie uma pasta no seu computador, e com o git Bash nela você envia os comandos:
- para iniciar

```
git init
```
- para clonar o projeto para sua maquina
```
git clone https://gitlab.com/claudioasj/civitastt.git
```
- Depois você pode abrir o terminal do VScode e digitar:
```
  npm install
```
- em seguida
 ```
 npx json-server --watch db.json
 ```
-  Finalmente, basta abrir o arquivo `index.html` na pasta pages com a extensão live server para iniciar a aplicação
  ### Tarefas:
1. tarefa de ui - landing page
2. tarefa de JavaScript III - criar um formulário
<br><br>
<div align="center">
  <font color=#8A2BE2>
  Desenvolvido por Claudio Jr 🌊
  </font>
</div>


